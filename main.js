//Steve Is Cool

const axios = require ("axios");
const { exec } = require("child_process");
const child_process = require("child_process");
const fs = require("fs");
const { rawListeners } = require("process");
const path = require('path');
let Client = require('ssh2-sftp-client');
let FTPClient = require('basic-ftp');
let steveConfig = require('./steveconfig.json');
let sftp = new Client();

const baseDir = "./repositories";

const minute = 60000;
const second = 1000;

/**
 * Steve fetches a json from SteveDashboard, with git repos / main branch / host / etc..
 * 
 * Steve monitors the git main branches. If one has updates, pull it and shove it into the (s)ftp/subfolder (create subfolder if not exist)
 * then post webhook with URL
 * 
 * TODO: Send something back to Steveboard when deployment fails?
 */


var repos = [];

console.log(steveConfig);

axios.get(steveConfig.steveboardAPI + "steveapi.php",
{
    headers:
    {
        key : steveConfig.steveboardAPIKey
    }
}).then(function(data)
{
    console.log(data.data);
    repos = [];
    data.data.forEach(raw => {
        repos.push(
            {
                name: raw.name,
                liveurl: raw.liveurl,
                userId: raw.userid,
                webhook: raw.webhook,
                buildscript: raw.buildscript,
                git:
                {
                    url: raw.giturl,
                    branch: raw.gitbranch,
                    buildfolder: raw.gitbuildfolder
                },
                sftp:
                {
                    host: raw.sftphost,
                    port: raw.sftpport,
                    username: raw.sftpusername,
                    password: raw.sftppassword,
                    directory: raw.sftpdirectory
                }
            }
        )
    });

    CheckRepository(0);
}).catch(err => {console.log(err)})




function GetFolderStructure(gitUrl) 
{
    let split = gitUrl.split("://")[1];
    split = split.split("/");
    split[split.length - 1] = split[split.length - 1].split(".")[0];
    return split.join("/") + "/";
}

function makePath(dir) {
    fs.mkdirSync(dir, {recursive: true})
}

function gitOptions(dir)
{
    return "--git-dir=" + dir + "/.git --work-tree=" + dir;
}

function CheckRepository(i)
{
    next = function()
    {
        i++;

        if(i == repos.length)
        {
            console.log("Done!\nAwaiting next loop")
            setTimeout(() => {
                Crash();
            }, minute);
        }
        else
            CheckRepository(i)
    }

    try {
        doGit(repos[i], next);
    } catch(error) {
        console.error(error);
    }

}

function doGit(repo, next) {
    let timeoutMinutes = 1;
    let dir = baseDir + "/" + GetFolderStructure(repo.git.url);
    console.log("Starting job for " + repo.name)
    makePath(dir);
    console.log("Path created: " + dir)
    exec('git ' + gitOptions(dir) + ' fetch --all', {timeout: minute * timeoutMinutes}, function(err, stdout, stderr) 
    {
        console.log("Git fetched")
        exec('git ' + gitOptions(dir) + ' log --oneline ' + repo.git.branch + '..origin/' + repo.git.branch, {timeout: minute * timeoutMinutes}, function(err, stdout, stderr) 
        {
            console.log("Log retrieved")

            if(stdout !== "")
            {
                console.log(stdout);
                exec( 'git '  + gitOptions(dir) + ' checkout .', {timeout: minute * timeoutMinutes}, function() 
                {
                    exec( 'git '  + gitOptions(dir) + ' pull', {timeout: minute * timeoutMinutes}, function() 
                    {
                        DoBuild(repo, dir, stdout, stdout, next);
                    });
                });
            }
            else if(stderr.includes("not a git repository")) 
            {
                console.log("No repo found")
                console.log(stdout);
                exec( 'git clone ' + repo.git.url + ' ' + dir, {timeout: minute * timeoutMinutes}, function()
                {
                    console.log("Git cloned")
                    //checkout to git.branch
                    exec( 'git ' + gitOptions(dir) + ' checkout ' + repo.git.branch, {timeout: minute * timeoutMinutes}, function(err, stdout, stderr) 
                    {
                        let change = stdout;
                        exec( 'git ' + gitOptions(dir) + ' log --oneline ' + repo.git.branch, {timeout: minute * timeoutMinutes}, function(err, stdout, stderr) 
                        {
                            DoBuild(repo, dir, change, stdout, next);
                        });
                    });
                });
            }
            else
            {
                console.log("No changes")
                console.log(stdout);
                console.log(stderr);
                console.log(err);
                next();
            }
        });
    });
}

function DoBuild(repo, dir, change, log, callback)
{
    console.log("Checking build script...")
    if(repo.buildscript)
    {
        console.log(dir + path.dirname(repo.buildscript))
        console.log(dir + repo.buildscript)
    }

    if(repo.buildscript && fs.existsSync(dir + path.dirname(repo.buildscript)))
    {
        console.log("Build script exists");
        try {
            execBuild(repo, dir, change, log, callback);
        } catch(error) {
            console.error(error);
        }
    }
    else
    {
        console.log("Build script does not exist");
        DoUpload(repo, dir, change, log, callback);
    }
}

function execBuild(repo, dir, change, log, callback) {
    return new Promise((resolve, reject) => {
        exec("bash \"" + repo.buildscript + "\"", {cwd: dir}, 
        function (error, stdout, stderror)
        {
            if(error || stderror)
            {
                console.log("BUILD ERROR")
                let msg = {
                    username: "Steve",
                    avatar_url: steveConfig.steveboardAPI + "images/steveDiscord.png",
                    embeds: 
                    [{
                        title: "Build Error for " + repo.name.trim() + "!",
                        description: "Error Details:```" + error + "\n" + stderror + "```",
                        color: 16711680
                    }]
                }
                axios.post("https://discord.com/api/webhooks/968613111785467915/72mc147OCnm-amIUcNdBjMQafQRj6wyjFplmpfmaygepFBuzBufBW9zYRSRko6KmuMRT", //TODO: Make configurable in SteveDashboard
                msg)
                reject(error);
            }

            try 
            {
                fs.writeFileSync(dir + repo.git.buildfolder + "/stevebuildlog.txt", ((error!= null? "Error:\n" + error.toString() + "\n\n":"") + (stdout!= null && stdout.length>0? "stdout:\n" + stdout + "\n\n":"") + (stderror!= null && stderror.length>0? "stderror:\n" + stderror:"")).trim());
            } 
            catch (error) 
            {
                console.log("Caught error writing log:", error)
                console.log("Skipping, no issue");
            }
            
            DoUpload(repo, dir, change, log, callback);

            resolve();
        });

    });
}

function DoUpload(repo, dir, change, log, callback)
{
    let rss;
    if(log != null && log.length > 0 && log.split)
    {
        log = log.split("\n")
        rss = "<rss version=\"2.0\"><channel><title>" + repo.name + "</title><description>Deployment Feed by Steve</description><link>" + repo.liveurl + "</link>";
        for(var i = log.length - 1; i >= 0; i--)
        {
            rss += "<item><title>New Build Live for " + repo.name + "!</title><description>" + log[i] + "</description><link>" + repo.liveurl + "</link>" + "</item>"
        }
        rss += "</channel></rss>";
    }

    let buildFolder = repo.git.buildfolder;

    /**
     * IF buildtype = ZIP ( https://javascript.plainenglish.io/how-to-create-zip-files-with-node-js-505e720ceee1 )
     * makedir repo/stevegeneratedzip/                                          <nieuwe dir om zip in te stoppen
     * MakeZIP(repo/buildfolder, repo/stevegeneratedzip/projectname.zip)        <vervang spaties met underscore?
     * buildfolder = repo/stevegeneratedzip                                     <voor upload code
     */

    if(rss)
    {
        try
        {
        console.log("writing RSS to " + dir + buildFolder + "/rss.xml");
        fs.writeFileSync(dir + buildFolder + "/rss.xml", rss);
        }
        catch(e)
        {
            console.log("no RSS I guess")
        }
    }

    switch(repo.sftp.port)
    {
        case '22': DoSFTP(repo, buildFolder, dir, log, callback);
            break;
        case '21': DoFTP(repo, buildFolder, dir, log, callback);
            break;
        default:
            console.log("Protocol for port " + repo.sftp.port + " not supported!");
    }

    try {
        fs.unlinkSync(dir + buildFolder + "/rss.xml");
    } catch (error) {
        console.log("no RSS")
    }
    
}

async function DoFTP(repo, buildFolder, dir, change, callback) 
{
    const ftpclient = new FTPClient.Client();

    try
    {
        await ftpclient.access({
            host: repo.sftp.host,
            user: repo.sftp.username,
            password: repo.sftp.password,
            secure: true,
            secureOptions: {checkServerIdentity: () => undefined}
        });
        await ftpclient.ensureDir(repo.sftp.directory);
        await ftpclient.clearWorkingDir();
        await ftpclient.uploadFromDir(dir + buildFolder);

        generateDiscordMessage(repo, repo.webhook, change);
    }
    catch(err)
    {
        console.log(err);
    }
    
    ftpclient.close();
    callback && callback();
}

function DoSFTP(repo, buildFolder, dir, change, callback)
{
    console.log(repo);
    sftp.connect({
        host: repo.sftp.host,
        port: repo.sftp.port,
        username: repo.sftp.username,
        password: repo.sftp.password
    }).then(() => {
        return sftp.exists(repo.sftp.directory);
    }).then(data => {
        if(data != false)
            return sftp.rmdir(repo.sftp.directory, true);
    }).then(() => {
        return sftp.uploadDir(dir + buildFolder, repo.sftp.directory);
    }).then(() => {
        generateDiscordMessage(repo, repo.webhook, change);
        callback && callback();
        return sftp.end();
    }).catch(err => {
        console.error(err.message);
        callback && callback();
        return sftp.end();
    });

}

function MakeZIP(folder, zipname)
{
    child_process.execSync("zip -r " + zipname + " *", {
        cwd: folder
    });
}

function generateDiscordMessage(repo, extraUrl, change)
{
    if(change.split)
        change = change.split("\n");

    if(change.reverse)
        change = change.reverse();

    if(change.join)
        change = change.join("\n");
    
    /**
     * Configurable webhooks:
     * 1: 'main' Steve hook; all changes go here
     * 2: additional webhook configured per-project
     */

    let msg = {
        username: "Steve",
        avatar_url: steveConfig.steveboardAPI + "images/steveDiscord.png",
        embeds: 
        [{
            title: "New build pushed for " + repo.name.trim() + "!",
            description: "Changes:```" + change + "```\nView the build here: " + repo.liveurl,
            color: 1127128
        }]
    }
    axios.post("https://discord.com/api/webhooks/968613111785467915/72mc147OCnm-amIUcNdBjMQafQRj6wyjFplmpfmaygepFBuzBufBW9zYRSRko6KmuMRT", //TODO: Make configurable in SteveDashboard
    msg)

    if(repo.webhook != null && repo.webhook.length > 0)
    {
        axios.post(extraUrl, msg)
    }
}

